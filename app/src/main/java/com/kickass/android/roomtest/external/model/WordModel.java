package com.kickass.android.roomtest.external.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.kickass.android.roomtest.domain.entity.Word;

@Entity(tableName = "word_table")
public class WordModel {
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "word")
    private String mWord;

    @ColumnInfo(name = "meaning")
    private String mMeaning;

    public WordModel(@NonNull String word, @NonNull String meaning) {
        this.mWord = word;
        this.mMeaning = meaning;
    }

    public String getWord(){return this.mWord;}

    public String getMeaning() {
        return this.mMeaning;
    }

    public WordModel(@NonNull Word word) {
        this.mWord = word.mWord;
        this.mMeaning = word.mMeaning;
    }

    @Override
    public String toString() {
        return this.mWord + ": " + this.mMeaning;
    }
}
