package com.kickass.android.roomtest.domain.usecase;

public class AddOperator extends AbsOperator {
    public AddOperator(WordOperationArgs args) {
        super(args);
    }

    @Override
    public void prepare() {
        mWordOperation.prepareOperation(mType, mWordValue, mWordMeaning);
    }


    @Override
    public WordOperationArgs.Result execute() {
        WordOperationArgs.Result result = new WordOperationArgs.Result();
        try {
            result.mIsSuccess = mWordOperation.add(mWordValue, mWordMeaning);
        } catch (Exception e) {
            result.mIsSuccess = false;
            result.mException = e;
        }
        return result;
    }

    @Override
    public void finish(WordOperationArgs.Result result) {
        mWordOperation.finishOperation();
    }
}
