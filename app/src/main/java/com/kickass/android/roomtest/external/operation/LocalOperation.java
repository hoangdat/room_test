package com.kickass.android.roomtest.external.operation;

import android.content.Context;

import com.kickass.android.roomtest.domain.entity.Word;
import com.kickass.android.roomtest.domain.usecase.AbsWordOperation;
import com.kickass.android.roomtest.domain.usecase.WordOperationArgs;
import com.kickass.android.roomtest.external.database.repository.WordRepository;
import com.kickass.android.roomtest.external.log.Log;

public class LocalOperation extends AbsWordOperation {
    private WordRepository mWordRepository;

    public LocalOperation(Context applicationContext) {
        mWordRepository = new WordRepository(applicationContext.getApplicationContext());
    }

    @Override
    public boolean repair(String word, String newWord) throws Exception {
        return false;
    }

    @Override
    public boolean add(String word, String meaning) throws Exception {
        Log.d(this, "add: w = " + word + "- m = " + meaning);
        return mWordRepository.insert(new Word(word, meaning));
    }

    @Override
    public boolean delete(String word) throws Exception {
        return false;
    }

    @Override
    public void prepareOperation(WordOperationArgs.WordOperationType type, String word, String meaning) {
        Log.d(this, "prepareOperation: type = " + type + " w = " + word + "- m = " + meaning);
    }

    @Override
    public void finishOperation() {
        Log.d(this, "finishOperation");
    }
}
