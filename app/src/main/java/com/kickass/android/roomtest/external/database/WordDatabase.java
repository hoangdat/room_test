package com.kickass.android.roomtest.external.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.kickass.android.roomtest.external.database.dao.WordDao;
import com.kickass.android.roomtest.external.model.WordModel;

@Database(entities = {WordModel.class}, version = 1)
public abstract class WordDatabase extends RoomDatabase {
    private static WordDatabase sInstance;

    public static WordDatabase getDatabase(final Context context) {
        if (sInstance == null) {
            synchronized (WordDatabase.class) {
                if (sInstance == null) {
                    sInstance = Room.databaseBuilder(context.getApplicationContext(),
                            WordDatabase.class, "dict.db")
                            .build();
                }
            }
        }
        return sInstance;
    }

    public abstract WordDao wordDao();
}
