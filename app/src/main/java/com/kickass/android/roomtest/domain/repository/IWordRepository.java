package com.kickass.android.roomtest.domain.repository;

import com.kickass.android.roomtest.domain.entity.Word;

import java.util.List;

public interface IWordRepository<T extends Word> {
    List<T> search(String query);

    List<T> getWordList();

    boolean insert(Word word);
}
