package com.kickass.android.roomtest.domain.usecase.outputport;

import com.kickass.android.roomtest.domain.usecase.WordOperationArgs;

public interface IWordOperationResultListener {
    void onWordOperationFinished(WordOperationArgs.Result result);
}
