package com.kickass.android.roomtest.external.database.repository;

import android.arch.lifecycle.LiveData;
import android.content.Context;

import com.kickass.android.roomtest.domain.entity.Word;
import com.kickass.android.roomtest.external.database.WordDatabase;
import com.kickass.android.roomtest.external.database.dao.WordDao;
import com.kickass.android.roomtest.external.log.Log;
import com.kickass.android.roomtest.external.model.WordModel;
import com.kickass.android.roomtest.presenter.repository.AbsRepository;

import java.util.List;

public class WordRepository extends AbsRepository {
    private WordDao mWordDao;
    private LiveData<List<WordModel>> mAllWords;

    @Override
    public List<Word> search(String query) {
        return null;
    }

    @Override
    public List<Word> getWordList() {
        return null;
    }

    @Override
    public boolean insert(Word word) {
        boolean ret = false;
        if (mWordDao != null) {
            WordModel model = new WordModel(word);
            Long id = mWordDao.insert(model);
            if (id != null && id >= 0) {
                Log.d(this, "insert " + model);
                ret = true;
            }
        }
        return ret;
    }


    public WordRepository(Context applicationContext) {
        WordDatabase db = WordDatabase.getDatabase(applicationContext);
        mWordDao = db.wordDao();
        mAllWords = mWordDao.getAllWords();
    }

    public LiveData<List<WordModel>> getAllWords() {
        return mAllWords;
    }
}
