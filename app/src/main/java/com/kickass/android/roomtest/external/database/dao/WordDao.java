package com.kickass.android.roomtest.external.database.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.kickass.android.roomtest.external.model.WordModel;

import java.util.List;

@Dao
public interface WordDao {
    @Insert
    Long insert(WordModel word);

    @Query("DELETE FROM word_table")
    void deleteAll();

    @Query("SELECT * FROM word_table ORDER BY word ASC")
    LiveData<List<WordModel>> getAllWords();
}
