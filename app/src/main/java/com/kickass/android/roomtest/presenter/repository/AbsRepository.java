package com.kickass.android.roomtest.presenter.repository;

import android.arch.lifecycle.LiveData;

import com.kickass.android.roomtest.domain.entity.Word;
import com.kickass.android.roomtest.domain.repository.IWordRepository;
import com.kickass.android.roomtest.external.model.WordModel;

import java.util.List;

public abstract class AbsRepository implements IWordRepository<Word> {
    public abstract LiveData<List<WordModel>> getAllWords();
}
