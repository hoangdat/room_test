package com.kickass.android.roomtest.domain.usecase;

import com.kickass.android.roomtest.domain.usecase.outputport.IWordOperation;
import com.kickass.android.roomtest.domain.usecase.outputport.IWordOperationResultListener;

public abstract class AbsOperator {
    protected IWordOperationResultListener mResultListener;
    protected IWordOperation mWordOperation;
    protected String mWordValue;
    protected String mWordMeaning;
    protected WordOperationArgs.WordOperationType mType;

    public AbsOperator(WordOperationArgs args) {
        mResultListener = args.mResultListener;
        mWordOperation = args.mWordOperator;
        mWordValue = args.mWordValue;
        mWordMeaning = args.mWordMeaning;
        mType = args.mWordType;
    }

    public abstract void prepare();

    public abstract WordOperationArgs.Result execute();

    public abstract void finish(WordOperationArgs.Result result);

    public void notifyResult(WordOperationArgs.Result result) {
        if (mResultListener != null) {
            mResultListener.onWordOperationFinished(result);
        }
    }
}
