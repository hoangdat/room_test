package com.kickass.android.roomtest.external.operation;

import android.content.Context;
import android.util.SparseArray;

import com.kickass.android.roomtest.domain.usecase.outputport.IWordOperation;

public class OperationManager {
    private static SparseArray<IWordOperation> sWordOperationMapByInstances = new SparseArray<>();

    private static void initOperationList(Context context, int instanceId) {
        sWordOperationMapByInstances.put(instanceId, new LocalOperation(context));
    }

    public static IWordOperation getOperation(Context context, int instanceId) {
        if (sWordOperationMapByInstances.get(instanceId) == null) {
            initOperationList(context, instanceId);
        }
        return sWordOperationMapByInstances.get(instanceId);
    }

    public enum OperationType {
        OPERATION_NONE(0),
        OPERATION_LOCAL(1);

        private int mValue;

        OperationType(int i) {
            mValue = i;
        }

        public int getValue() {
            return mValue;
        }

        public static OperationType fromInt(int i) {
            for (OperationType type : OperationType.values()) {
                if (type.getValue() == i) {
                    return type;
                }
            }
            return null;
        }
    }
}
