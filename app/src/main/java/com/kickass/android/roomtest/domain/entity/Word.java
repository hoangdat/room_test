package com.kickass.android.roomtest.domain.entity;

public class Word {
    public Word(String word, String meaning) {
        mWord = word;
        mMeaning = meaning;
    }

    public String mWord;

    public String mMeaning;

    @Override
    public String toString() {
        return mWord + ": " + mMeaning;
    }
}
