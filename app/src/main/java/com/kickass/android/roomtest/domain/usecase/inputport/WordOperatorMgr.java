package com.kickass.android.roomtest.domain.usecase.inputport;

import com.kickass.android.roomtest.domain.usecase.AbsOperator;
import com.kickass.android.roomtest.domain.usecase.AddOperator;
import com.kickass.android.roomtest.domain.usecase.DeleteOperator;
import com.kickass.android.roomtest.domain.usecase.RepairOperator;
import com.kickass.android.roomtest.domain.usecase.ThreadExecutor;
import com.kickass.android.roomtest.domain.usecase.WordOperationArgs;

public class WordOperatorMgr {
    private AbsOperator mFileOperator;

    public WordOperatorMgr(WordOperationArgs args) {
        switch (args.mWordType) {
            case ADD:
                mFileOperator = new AddOperator(args);
                break;
            case DELETE:
                mFileOperator = new DeleteOperator(args);
                break;
            case REPAIR:
                mFileOperator = new RepairOperator(args);
                break;
            default:
                break;
        }
    }

    public void execute() {
        ThreadExecutor.getInstance().execute(new Runnable() {
            @Override
            public void run() {
                final WordOperationArgs.Result result;
                mFileOperator.prepare();
                result = mFileOperator.execute();
                mFileOperator.finish(result);
                ThreadExecutor.getInstance().mainThread().execute(new Runnable() {
                    @Override
                    public void run() {
                        mFileOperator.notifyResult(result);
                    }
                });
            }
        });
    }
}
