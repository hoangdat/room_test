package com.kickass.android.roomtest.presenter.controller;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.support.annotation.NonNull;

import com.kickass.android.roomtest.domain.entity.Word;
import com.kickass.android.roomtest.external.database.repository.WordRepository;
import com.kickass.android.roomtest.external.model.WordModel;
import com.kickass.android.roomtest.presenter.repository.AbsRepository;

import java.util.List;

public class MainController extends AndroidViewModel {
    private AbsRepository mRepository;

    public MainController(@NonNull Application application) {
        super(application);
        mContext = application.getApplicationContext();
        mRepository = new WordRepository(mContext);
        mListItems = mRepository.getAllWords();
    }

    protected final Context mContext;

    private LiveData<List<WordModel>> mListItems;

    public LiveData<List<WordModel>> getListItems() {
        return mListItems;
    }
}
