package com.kickass.android.roomtest.domain.usecase;

public class DeleteOperator extends AbsOperator {
    public DeleteOperator(WordOperationArgs args) {
        super(args);
    }

    @Override
    public void prepare() {

    }

    @Override
    public WordOperationArgs.Result execute() {
        WordOperationArgs.Result result = new WordOperationArgs.Result();
        try {
            result.mIsSuccess = mWordOperation.delete(mWordValue);
        } catch (Exception e) {
            result.mIsSuccess = false;
            result.mException = e;
        }
        return result;
    }

    @Override
    public void finish(WordOperationArgs.Result result) {

    }
}
