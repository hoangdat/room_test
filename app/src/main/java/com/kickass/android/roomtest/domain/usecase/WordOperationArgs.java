package com.kickass.android.roomtest.domain.usecase;

import com.kickass.android.roomtest.domain.usecase.outputport.IWordOperation;
import com.kickass.android.roomtest.domain.usecase.outputport.IWordOperationResultListener;

public class WordOperationArgs {

    public WordOperationType mWordType = WordOperationType.NONE;

    public IWordOperationResultListener mResultListener;

    public IWordOperation mWordOperator;

    public String mWordValue;

    public String mWordMeaning;

    public enum WordOperationType {
        ADD,
        DELETE,
        REPAIR,
        NONE
    }

    public WordOperationArgs() {
    }

    public static class Result {
        public boolean mIsSuccess;
        public Exception mException;
    }
}
