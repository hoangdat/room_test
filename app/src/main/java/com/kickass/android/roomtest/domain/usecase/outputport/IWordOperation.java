package com.kickass.android.roomtest.domain.usecase.outputport;

import com.kickass.android.roomtest.domain.usecase.WordOperationArgs;

public interface IWordOperation {
    boolean repair(String word, String newWord) throws Exception;

    boolean add(String word, String meaning) throws Exception;

    boolean delete(String word) throws Exception;

    void prepareOperation(WordOperationArgs.WordOperationType type, String word, String meaning);

    void finishOperation();
}
