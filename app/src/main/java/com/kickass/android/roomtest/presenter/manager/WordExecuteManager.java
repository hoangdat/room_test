package com.kickass.android.roomtest.presenter.manager;

import com.kickass.android.roomtest.domain.usecase.WordOperationArgs;
import com.kickass.android.roomtest.domain.usecase.inputport.WordOperatorMgr;
import com.kickass.android.roomtest.domain.usecase.outputport.IWordOperation;
import com.kickass.android.roomtest.domain.usecase.outputport.IWordOperationResultListener;

public class WordExecuteManager {
    public void execute(WordOperationArgs.WordOperationType type, IWordOperation operation, String word, String meaning, final ExecuteResultListener listener) {
        WordOperationArgs params = new WordOperationArgs();
        params.mWordType = type;
        params.mWordValue = word;
        params.mWordMeaning = meaning;
        params.mResultListener = new IWordOperationResultListener() {
            @Override
            public void onWordOperationFinished(WordOperationArgs.Result result) {
                listener.onExecuted(result);
            }
        };
        params.mWordOperator = operation;
        WordOperatorMgr operatorMgr = new WordOperatorMgr(params);
        operatorMgr.execute();
    }

    public interface ExecuteResultListener {
        void onExecuted(WordOperationArgs.Result result);
    }
}
