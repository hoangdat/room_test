package com.kickass.android.roomtest.external;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import com.kickass.android.roomtest.R;
import com.kickass.android.roomtest.domain.usecase.WordOperationArgs;
import com.kickass.android.roomtest.external.log.Log;
import com.kickass.android.roomtest.external.model.WordModel;
import com.kickass.android.roomtest.external.operation.OperationManager;
import com.kickass.android.roomtest.presenter.controller.MainController;
import com.kickass.android.roomtest.presenter.manager.WordExecuteManager;

import java.util.List;

public class Main2Activity extends AppCompatActivity {
    private EditText mEdtWord, mEdtMeaning;
    private MainController mController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mEdtWord = (EditText) findViewById(R.id.txtWord);
        mEdtMeaning = (EditText) findViewById(R.id.txtMeaning);

        mController = ViewModelProviders.of(this).get(MainController.class);
        mController.getListItems().observe(this, new Observer<List<WordModel>>() {
            @Override
            public void onChanged(@Nullable List<WordModel> words) {
                Log.d(this, "onChanged: size = " + words.size());
                for (WordModel word : words) {
                    Log.d(this, "onChanged: " + word);
                }
            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkConditionAdd()) {
                    String word = mEdtWord.getText().toString();
                    String meaning = mEdtMeaning.getText().toString();

                    WordExecuteManager exeMgr = new WordExecuteManager();
                    exeMgr.execute(WordOperationArgs.WordOperationType.ADD,
                            OperationManager.getOperation(getApplicationContext(), 1),
                            word, meaning, new WordExecuteManager.ExecuteResultListener() {
                        @Override
                        public void onExecuted(WordOperationArgs.Result result) {
                            Log.d(this, "onExecuted: " + result.mIsSuccess);
                        }
                    });

                    mEdtMeaning.setText("");
                    mEdtWord.setText("");
                }
            }
        });
    }

    private boolean checkConditionAdd() {
        return !(TextUtils.isEmpty(mEdtWord.getText().toString()) || TextUtils.isEmpty(mEdtMeaning.getText().toString()));
    }

}
